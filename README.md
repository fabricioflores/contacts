# contacts

a sails-react application

### development

- clone the app
- npm install
- npm start (will upload sails and webpack with the frontend)

### testing

- npm run test: will run a linter and mocha, with istambul as a coverage tool.

### links

Running app http://ec2-34-238-235-81.compute-1.amazonaws.com/

### coding challenger

- SPA in React, must be hosted in a SailsJS server in a EC2 instance, with Linux either Amazon Linux or CentOS DONE

- Sails will host the React SPA. DONE

- DB needs to be Aurora Serverless. DONE

- Bootstrap 3 or 4. DONE

### coding challenger details

- A user enters the main page. I will show the list of phones, names and a thumbnail. This data must come from SailsJS, your React app will consume the data from Sails passing through an API Gateway GET endpoint. DONE

- Put an endpoint on API Gateway. DONE

- API Gateway will be the front of a Sails endpoint. DONE

- A user can create a new record. I can add the name, phone and a original image. DONE

- Once I submit a record, I will send it to an API Gateway POST endpoint, which will be backed by a Lambda function. DONE

- The Lambda function will receive the image and will resize the image to be 128x128. DONE

- Lambda will upload both the original and the resized image to a S3 bucket. DONE

- Lambda will add the record to Aurora Serverless. DONE

- All images come S3. DONE

- ES6. DONE

- Node. DONE

- All the code must have tests and code coverage. DONE

### implementation details

- The app is hosted in a EC2 instance

- The react app is the frontend of the sails app. In development it is running with webpack, in deployment we just need to do a `npm run build` and it will be copied in the index of the sails app.

- RDS, S3 and Lambda are being used.

- All the secrets are stored as env vars.

- In deployment, I'm using pm2.

### aws implementation details

- For ec2 instances, I created a new instance and a new security group, with the 80 port open. I used nginx as a proxy between the 80 port and the 8080 port (custom port I used to deploy instance).

- For RDS aurora instance, I created a new instance and a new security group, with the 3306 port open.

- For a public bucket in S3 I added a directive to prevent the forbidden error.

- For Lambda, I created a subfolder ./lambda and write the code inside. Also, I needed to create a package JSON to use `mysql` and `file-type` libraries.

- Also, for Lambda I created a script to zip and upload the code.

- About the API Gateway for the Lambda function, is is getting `application/json` type. From lamda I'm getting the created record to show it in the UI.
