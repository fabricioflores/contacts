const AWS = require('aws-sdk');
const fileType = require('file-type');
const database = require('./database');

const s3 = new AWS.S3();

const imageTypes = [
  'image/gif',
  'image/jpeg',
  'image/png'
];

exports.handler = (event, context, callback) => {
  const body = event;

  if(!body.name || !body.phone || !body.email){
    return callback('Missing info', null);
  }

  const connection = database.connectDB();

  if(body['image']){
    const fileBuffer = new Buffer(body['image'], 'base64');
    const fileTypeInfo = fileType(fileBuffer);
    if(!fileTypeInfo){
      return callback('Invalid file type', null);
    }
    if (fileBuffer.length < 500000 && imageTypes.includes(fileTypeInfo.mime)) {

      const fileName = `${Math.floor(new Date() / 1000)}.${fileTypeInfo.ext}`;

      const bucket = process.env.BUCKET;
      const params = {
        Body: fileBuffer,
        Key: fileName,
        Bucket: bucket,
        ContentEncoding: 'base64',
        ContentType: fileTypeInfo.mime
      };
      s3.upload(params).promise().then((data) => {
        const imageUrl = data.Location;
        body.image = imageUrl;
        database.insertDB(connection, body, callback);
      }, (error) => {
        callback(error,null);
      })
    } else {
      callback('Not a valid file type or file too big.', null);
    }
  }else{
    database.insertDB(connection, body, callback);
  }

};
