const mysql = require('mysql');

exports.connectDB = () => {
  return mysql.createConnection({
    host: process.env.dbHost,
    user: process.env.dbUser,
    password: process.env.dbPassword,
    database: process.env.dbName,
  });
}

exports.insertDB = (connection, body, callback) => {
  connection.query('INSERT INTO contact SET ?', body, function (error, results) {
    if (error) {
        connection.destroy();
        callback(error, null);
    } else {
        body.id = results.insertId;
        callback(null, body);
    }
    connection.end(function (error) { callback(error, results);});
  });
}
