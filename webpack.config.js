const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');

module.exports = {
  entry: {
    entry: './client/index.js'
  },
  output: {
    path: __dirname + '/.tmp/public',
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        use: 'babel-loader',
        test: /\.js$/,
        exclude: /node_modules/
      },
      {
        exclude: [/\.(js|jsx|mjs)$/, /\.html$/, /\.json$/,  /\.css$/],
        loader: require.resolve('file-loader'),
        options: {
          name: 'static/media/[name].[hash:8].[ext]',
        },
      },
      { test: /\.css$/, loader: 'style-loader!css-loader'},
    ]
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: 'client/index.html'
    })
  ]
};
