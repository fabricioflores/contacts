rm index.zip
cd lambda
zip ../index.zip * -r -X
cd ..
aws lambda update-function-code --function-name resize-and-upload --zip-file fileb://index.zip
